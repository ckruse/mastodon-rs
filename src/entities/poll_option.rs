use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PollOption {
    pub title: String,
    pub votes_count: i64,
}
