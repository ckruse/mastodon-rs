use serde::{Deserialize, Serialize};

use super::{MediaType, Meta};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Attachment {
    pub id: String,
    #[serde(rename = "type")]
    pub media_type: MediaType,
    pub url: String,
    pub preview_url: String,

    pub remote_url: Option<String>,
    pub meta: Option<Meta>,
    pub description: Option<String>,
    pub blurhash: Option<String>,

    // deprecated
    pub text_url: Option<String>,
}
