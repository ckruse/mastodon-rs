use serde::{Deserialize, Serialize};

use super::{Field, Visibility};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Source {
    pub note: String,
    pub fields: Vec<Field>,
    pub privacy: Option<Visibility>,
    pub sensitive: Option<bool>,
    pub language: Option<String>,
    pub follow_requests_count: Option<i64>,
}
