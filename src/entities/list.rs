use serde::{Deserialize, Serialize};

use super::RepliesPolicy;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct List {
    pub id: String,
    pub title: String,
    pub replies_policy: RepliesPolicy,
}
