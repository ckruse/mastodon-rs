use serde::{Deserialize, Serialize};

pub mod account;
pub mod application;
pub mod attachment;
pub mod card;
pub mod context;
pub mod conversation;
pub mod emoji;
pub mod field;
pub mod focal_point;
pub mod history;
pub mod image_details;
pub mod list;
pub mod mention;
pub mod meta;
pub mod poll;
pub mod poll_option;
pub mod source;
pub mod status;
pub mod tag;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum MediaType {
    #[serde(rename = "image")]
    Image,
    #[serde(rename = "video")]
    Video,
    #[serde(rename = "audio")]
    Audio,
    #[serde(rename = "gifv")]
    Gifv,
    #[serde(rename = "unknown")]
    Unknown,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum LinkType {
    #[serde(rename = "link")]
    Link,
    #[serde(rename = "photo")]
    Photo,
    #[serde(rename = "video")]
    Video,
    #[serde(rename = "rich")]
    Rich,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,
    #[serde(rename = "unlisted")]
    Unlisted,
    #[serde(rename = "private")]
    Private,
    #[serde(rename = "direct")]
    Direct,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum RepliesPolicy {
    #[serde(rename = "followed")]
    Followed,
    #[serde(rename = "list")]
    List,
    #[serde(rename = "none")]
    None,
}

pub use account::Account;
pub use application::Application;
pub use attachment::Attachment;
pub use card::Card;
pub use context::Context;
pub use conversation::Conversation;
pub use emoji::Emoji;
pub use field::Field;
pub use focal_point::FocalPoint;
pub use history::History;
pub use image_details::ImageDetails;
pub use list::List;
pub use mention::Mention;
pub use meta::Meta;
pub use poll::Poll;
pub use poll_option::PollOption;
pub use source::Source;
pub use status::Status;
pub use tag::Tag;
