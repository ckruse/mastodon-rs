use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use super::{Emoji, Field, Source};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Account {
    pub id: String,

    pub username: String,
    pub acct: String,
    pub url: String,
    pub display_name: String,
    pub note: String,
    pub avatar: String,
    pub avatar_static: String,
    pub header: String,
    pub header_static: String,
    pub locked: bool,
    pub emojis: Vec<Emoji>,
    // surprisingly this can be null
    pub discoverable: Option<bool>,
    pub created_at: DateTime<Utc>,
    pub last_status_at: NaiveDate,
    pub statuses_count: u64,
    pub followers_count: u64,
    pub following_count: u64,

    pub moved: Option<Box<Account>>,
    pub fields: Vec<Field>,
    pub bot: Option<bool>,
    pub source: Option<Source>,
    pub suspended: Option<bool>,
    pub mute_expires_at: Option<DateTime<Utc>>,
}
