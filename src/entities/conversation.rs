use serde::{Deserialize, Serialize};

use super::{Account, Status};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Conversation {
    pub id: String,
    pub accounts: Vec<Account>,
    pub unread: bool,
    pub last_status: Option<Status>,
}
