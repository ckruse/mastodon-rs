use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct History {
    pub day: String,
    pub uses: String,
    pub accounts: String,
}
