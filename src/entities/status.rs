use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use super::{Account, Application, Attachment, Card, Emoji, Mention, Poll, Tag, Visibility};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Status {
    pub id: String,
    pub uri: String,
    pub created_at: DateTime<Utc>,
    pub account: Account,
    pub content: String,
    pub visibility: Visibility,
    pub sensitive: bool,
    pub spoiler_text: String,
    pub media_attachments: Vec<Attachment>,
    pub application: Option<Application>,

    pub mentions: Vec<Mention>,
    pub tags: Vec<Tag>,
    pub emojis: Vec<Emoji>,

    pub reblogs_count: u64,
    pub favourites_count: u64,
    pub replies_count: u64,

    pub url: Option<String>,
    pub in_reply_to_id: Option<String>,
    pub in_reply_to_account_id: Option<String>,
    pub reblog: Option<Box<Status>>,

    poll: Option<Poll>,
    pub card: Option<Card>,
    pub language: Option<String>,
    pub text: Option<String>,

    pub favourited: Option<bool>,
    pub reblogged: Option<bool>,
    pub muted: Option<bool>,
    pub bookmarked: Option<bool>,
    pub pinned: Option<bool>,
}
