use serde::{Deserialize, Serialize};

use super::{FocalPoint, ImageDetails};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Meta {
    pub original: Option<ImageDetails>,
    pub small: Option<ImageDetails>,
    pub focus: Option<FocalPoint>,
}
