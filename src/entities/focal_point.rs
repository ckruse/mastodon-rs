use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct FocalPoint {
    x: f64,
    y: f64,
}
