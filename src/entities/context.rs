use serde::{Deserialize, Serialize};

use super::Status;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Context {
    ancestors: Vec<Status>,
    descendants: Vec<Status>,
}
