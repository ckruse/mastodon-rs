use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use super::{Emoji, PollOption};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Poll {
    pub id: String,
    pub expires_at: DateTime<Utc>,
    pub expired: bool,
    pub multiple: bool,
    pub votes_count: i64,
    pub voters_count: i64,
    pub voted: Option<bool>,
    pub own_votes: Option<Vec<i64>>,
    pub options: Vec<PollOption>,
    pub emojis: Vec<Emoji>,
}
