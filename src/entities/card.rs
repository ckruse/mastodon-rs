use serde::{Deserialize, Serialize};

use super::LinkType;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Card {
    pub url: String,
    pub title: String,
    pub description: String,
    #[serde(rename = "type")]
    pub link_type: LinkType,

    pub author_name: Option<String>,
    pub author_url: Option<String>,
    pub provider_name: Option<String>,
    pub provider_url: Option<String>,
    pub html: Option<String>,
    pub width: Option<i64>,
    pub height: Option<i64>,
    pub image: Option<String>,
    pub embed_url: Option<String>,
    pub blurhash: Option<String>,
}
