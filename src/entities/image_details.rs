use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ImageDetails {
    pub width: u64,
    pub height: u64,
    // although not documented it looks like this can be missing
    pub size: Option<String>,
    // although not documented it looks like this can be missing
    pub aspect: Option<f64>,
}
