// OAuth scopes
//
// a lot of code is copied from elefren, a discontinued Mastodon client library.
// I changed the internal data structure, removed old permissions and added
// missing new permissions

use core::fmt;
use std::{cmp::Ordering, ops::BitOr, str::FromStr};

use anyhow::{bail, Error, Result};
use serde::{de, de::Visitor, Deserialize, Deserializer, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Read {
    #[serde(rename = "read:accounts")]
    Accounts,
    #[serde(rename = "read:blocks")]
    Blocks,
    #[serde(rename = "read:bookmarks")]
    Bookmarks,
    #[serde(rename = "read:accounts")]
    Favourites,
    #[serde(rename = "read:filters")]
    Filters,
    #[serde(rename = "read:follows")]
    Follows,
    #[serde(rename = "read:lists")]
    Lists,
    #[serde(rename = "read:mutes")]
    Mutes,
    #[serde(rename = "read:notifications")]
    Notifications,
    #[serde(rename = "read:search")]
    Search,
    #[serde(rename = "read:statuses")]
    Statuses,
}

impl FromStr for Read {
    type Err = Error;

    fn from_str(s: &str) -> Result<Read> {
        let rslt = match s {
            "accounts" => Read::Accounts,
            "blocks" => Read::Blocks,
            "favourites" => Read::Favourites,
            "filters" => Read::Filters,
            "follows" => Read::Follows,
            "lists" => Read::Lists,
            "mutes" => Read::Mutes,
            "notifications" => Read::Notifications,
            "search" => Read::Search,
            "statuses" => Read::Statuses,
            "bookmarks" => Read::Bookmarks,
            val => bail!("read category unknown: {}", val),
        };

        Ok(rslt)
    }
}

impl fmt::Display for Read {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "read:{}",
            match *self {
                Read::Accounts => "accounts",
                Read::Blocks => "blocks",
                Read::Favourites => "favourites",
                Read::Filters => "filters",
                Read::Follows => "follows",
                Read::Lists => "lists",
                Read::Mutes => "mutes",
                Read::Notifications => "notifications",
                Read::Search => "search",
                Read::Statuses => "statuses",
                Read::Bookmarks => "bookmarks",
            }
        )
    }
}

impl PartialOrd for Read {
    fn partial_cmp(&self, other: &Read) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Read {
    fn cmp(&self, other: &Read) -> Ordering {
        let a = format!("{:?}", self);
        let b = format!("{:?}", other);
        a.cmp(&b)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Write {
    #[serde(rename = "write:accounts")]
    Accounts,
    #[serde(rename = "write:blocks")]
    Blocks,
    #[serde(rename = "write:bookmarks")]
    Bookmarks,
    #[serde(rename = "write:conversations")]
    Conversations,
    #[serde(rename = "write:favourites")]
    Favourites,
    #[serde(rename = "write:filters")]
    Filters,
    #[serde(rename = "write:follows")]
    Follows,
    #[serde(rename = "write:lists")]
    Lists,
    #[serde(rename = "write:media")]
    Media,
    #[serde(rename = "write:mutes")]
    Mutes,
    #[serde(rename = "write:notifications")]
    Notifications,
    #[serde(rename = "write:reports")]
    Reports,
    #[serde(rename = "write:statuses")]
    Statuses,
}

impl FromStr for Write {
    type Err = Error;

    fn from_str(s: &str) -> Result<Write> {
        let rslt = match s {
            "accounts" => Write::Accounts,
            "blocks" => Write::Blocks,
            "favourites" => Write::Favourites,
            "filters" => Write::Filters,
            "follows" => Write::Follows,
            "lists" => Write::Lists,
            "media" => Write::Media,
            "mutes" => Write::Mutes,
            "notifications" => Write::Notifications,
            "bookmarks" => Write::Bookmarks,
            "statuses" => Write::Statuses,
            "conversations" => Write::Conversations,
            "reports" => Write::Reports,
            val => bail!("Write category unknown: {}", val),
        };

        Ok(rslt)
    }
}

impl fmt::Display for Write {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "write:{}",
            match *self {
                Write::Accounts => "accounts",
                Write::Blocks => "blocks",
                Write::Favourites => "favourites",
                Write::Filters => "filters",
                Write::Follows => "follows",
                Write::Lists => "lists",
                Write::Media => "media",
                Write::Mutes => "mutes",
                Write::Notifications => "notifications",
                Write::Bookmarks => "bookmarks",
                Write::Statuses => "statuses",
                Write::Conversations => "conversations",
                Write::Reports => "reports",
            }
        )
    }
}

impl PartialOrd for Write {
    fn partial_cmp(&self, other: &Write) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Write {
    fn cmp(&self, other: &Write) -> Ordering {
        let a = format!("{:?}", self);
        let b = format!("{:?}", other);
        a.cmp(&b)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum AdminRead {
    #[serde(rename = "read:accounts")]
    Accounts,
    #[serde(rename = "read:reports")]
    Reports,
}

impl FromStr for AdminRead {
    type Err = Error;

    fn from_str(s: &str) -> Result<AdminRead> {
        let rslt = match s {
            "accounts" => AdminRead::Accounts,
            "reports" => AdminRead::Reports,
            val => bail!("Admin read category unknown: {}", val),
        };

        Ok(rslt)
    }
}

impl fmt::Display for AdminRead {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "admin:read:{}",
            match *self {
                AdminRead::Accounts => "accounts",
                AdminRead::Reports => "reports",
            }
        )
    }
}

impl PartialOrd for AdminRead {
    fn partial_cmp(&self, other: &AdminRead) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for AdminRead {
    fn cmp(&self, other: &AdminRead) -> Ordering {
        let a = format!("{:?}", self);
        let b = format!("{:?}", other);
        a.cmp(&b)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum AdminWrite {
    #[serde(rename = "write:accounts")]
    Accounts,
    #[serde(rename = "write:reports")]
    Reports,
}

impl FromStr for AdminWrite {
    type Err = Error;

    fn from_str(s: &str) -> Result<AdminWrite> {
        let rslt = match s {
            "accounts" => AdminWrite::Accounts,
            "reports" => AdminWrite::Reports,
            val => bail!("Admin write category unknown: {}", val),
        };

        Ok(rslt)
    }
}

impl fmt::Display for AdminWrite {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "admin:write:{}",
            match *self {
                AdminWrite::Accounts => "accounts",
                AdminWrite::Reports => "reports",
            }
        )
    }
}

impl PartialOrd for AdminWrite {
    fn partial_cmp(&self, other: &AdminWrite) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for AdminWrite {
    fn cmp(&self, other: &AdminWrite) -> Ordering {
        let a = format!("{:?}", self);
        let b = format!("{:?}", other);
        a.cmp(&b)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Scope {
    #[serde(rename = "read")]
    Read(Option<Read>),
    #[serde(rename = "write")]
    Write(Option<Write>),
    #[serde(rename = "follow")]
    Follow,
    #[serde(rename = "push")]
    Push,
    #[serde(rename = "admin:read")]
    AdminRead(Option<AdminRead>),
    #[serde(rename = "admin:write")]
    AdminWrite(Option<AdminWrite>),
}

impl fmt::Display for Scope {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Scope::*;

        let s = match *self {
            Read(Some(ref r)) => return fmt::Display::fmt(r, f),
            Read(None) => "read",
            Write(Some(ref w)) => return fmt::Display::fmt(w, f),
            Write(None) => "write",
            Follow => "follow",
            Push => "push",
            AdminRead(Some(ref r)) => return fmt::Display::fmt(r, f),
            AdminRead(None) => "admin:read",
            AdminWrite(Some(ref r)) => return fmt::Display::fmt(r, f),
            AdminWrite(None) => "admin:write",
        };
        write!(f, "{}", s)
    }
}

impl FromStr for Scope {
    type Err = Error;

    fn from_str(s: &str) -> Result<Scope> {
        let rslt = match s {
            "read" => Scope::Read(None),
            "write" => Scope::Write(None),
            "admin:read" => Scope::AdminRead(None),
            "admin:write" => Scope::AdminWrite(None),
            "follow" => Scope::Follow,
            "push" => Scope::Push,
            read if read.starts_with("read:") => {
                let r: Read = Read::from_str(&read[5..])?;
                Scope::Read(Some(r))
            }
            write if write.starts_with("write:") => {
                let w: Write = Write::from_str(&write[6..])?;
                Scope::Write(Some(w))
            }
            read if read.starts_with("admin:read:") => {
                let r: AdminRead = AdminRead::from_str(&read[11..])?;
                Scope::AdminRead(Some(r))
            }
            write if write.starts_with("admin:write:") => {
                let r: AdminWrite = AdminWrite::from_str(&write[12..])?;
                Scope::AdminWrite(Some(r))
            }
            v => bail!("Unknown scope: {}", v),
        };

        Ok(rslt)
    }
}

impl Default for Scope {
    fn default() -> Self {
        Scope::Read(None)
    }
}

impl PartialOrd for Scope {
    fn partial_cmp(&self, other: &Scope) -> Option<Ordering> {
        Some(match (*self, *other) {
            (Scope::Read(None), Scope::Read(None)) => Ordering::Equal,
            (Scope::Read(None), Scope::Read(Some(..))) => Ordering::Less,
            (Scope::Read(Some(..)), Scope::Read(None)) => Ordering::Greater,
            (Scope::Read(Some(ref a)), Scope::Read(Some(ref b))) => a.cmp(b),

            (Scope::Write(None), Scope::Write(None)) => Ordering::Equal,
            (Scope::Write(None), Scope::Write(Some(..))) => Ordering::Less,
            (Scope::Write(Some(..)), Scope::Write(None)) => Ordering::Greater,
            (Scope::Write(Some(ref a)), Scope::Write(Some(ref b))) => a.cmp(b),

            (Scope::AdminRead(None), Scope::AdminRead(None)) => Ordering::Equal,
            (Scope::AdminRead(None), Scope::AdminRead(Some(..))) => Ordering::Less,
            (Scope::AdminRead(Some(..)), Scope::AdminRead(None)) => Ordering::Greater,
            (Scope::AdminRead(Some(ref a)), Scope::AdminRead(Some(ref b))) => a.cmp(b),

            (Scope::AdminWrite(None), Scope::AdminWrite(None)) => Ordering::Equal,
            (Scope::AdminWrite(None), Scope::AdminWrite(Some(..))) => Ordering::Less,
            (Scope::AdminWrite(Some(..)), Scope::AdminWrite(None)) => Ordering::Greater,
            (Scope::AdminWrite(Some(ref a)), Scope::AdminWrite(Some(ref b))) => a.cmp(b),

            (Scope::Read(..), Scope::AdminRead(..)) => Ordering::Less,
            (Scope::Read(..), Scope::AdminWrite(..)) => Ordering::Less,
            (Scope::Read(..), Scope::Write(..)) => Ordering::Less,
            (Scope::Read(..), Scope::Follow) => Ordering::Less,
            (Scope::Read(..), Scope::Push) => Ordering::Less,

            (Scope::Write(..), Scope::AdminRead(..)) => Ordering::Less,
            (Scope::Write(..), Scope::AdminWrite(..)) => Ordering::Less,
            (Scope::Write(..), Scope::Read(..)) => Ordering::Greater,
            (Scope::Write(..), Scope::Follow) => Ordering::Less,
            (Scope::Write(..), Scope::Push) => Ordering::Less,

            (Scope::AdminRead(..), Scope::AdminWrite(..)) => Ordering::Less,
            (Scope::AdminRead(..), Scope::Read(..)) => Ordering::Greater,
            (Scope::AdminRead(..), Scope::Write(..)) => Ordering::Greater,
            (Scope::AdminRead(..), Scope::Follow) => Ordering::Greater,
            (Scope::AdminRead(..), Scope::Push) => Ordering::Greater,

            (Scope::AdminWrite(..), Scope::AdminRead(..)) => Ordering::Greater,
            (Scope::AdminWrite(..), Scope::Read(..)) => Ordering::Greater,
            (Scope::AdminWrite(..), Scope::Write(..)) => Ordering::Greater,
            (Scope::AdminWrite(..), Scope::Follow) => Ordering::Greater,
            (Scope::AdminWrite(..), Scope::Push) => Ordering::Greater,

            (Scope::Follow, Scope::AdminWrite(..)) => Ordering::Greater,
            (Scope::Follow, Scope::AdminRead(..)) => Ordering::Greater,
            (Scope::Follow, Scope::Read(..)) => Ordering::Greater,
            (Scope::Follow, Scope::Write(..)) => Ordering::Greater,
            (Scope::Follow, Scope::Follow) => Ordering::Equal,
            (Scope::Follow, Scope::Push) => Ordering::Less,

            (Scope::Push, Scope::Push) => Ordering::Equal,
            (Scope::Push, _) => Ordering::Greater,
        })
    }
}

impl Ord for Scope {
    fn cmp(&self, other: &Scope) -> Ordering {
        PartialOrd::partial_cmp(self, other).unwrap()
    }
}

#[derive(Clone)]
pub struct Scopes {
    scopes: Vec<Scope>,
}

impl FromStr for Scopes {
    type Err = Error;

    fn from_str(s: &str) -> Result<Scopes> {
        let mut scopes = Vec::new();
        for scope in s.split_whitespace() {
            let scope = Scope::from_str(scope)?;
            scopes.push(scope);
        }

        Ok(Scopes { scopes })
    }
}

impl Serialize for Scopes {
    fn serialize<S>(&self, serializer: S) -> ::std::result::Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        let repr = format!("{}", self);
        serializer.serialize_str(&repr)
    }
}

struct DeserializeScopesVisitor;

impl<'de> Visitor<'de> for DeserializeScopesVisitor {
    type Value = Scopes;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(formatter, "space separated scopes")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Scopes::from_str(v).map_err(de::Error::custom)
    }
}

impl<'de> Deserialize<'de> for Scopes {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(DeserializeScopesVisitor)
    }
}

impl fmt::Debug for Scopes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        for scope in &self.scopes {
            write!(f, "{:?}", &scope)?;
        }
        Ok(write!(f, "]")?)
    }
}

impl Scopes {
    pub fn all() -> Scopes {
        Scopes::read_all() | Scopes::write_all() | Scopes::follow() | Scopes::push()
    }

    pub fn all_with_admin() -> Scopes {
        Scopes::read_all()
            | Scopes::write_all()
            | Scopes::follow()
            | Scopes::push()
            | Scopes::admin_read_all()
            | Scopes::admin_write_all()
    }

    pub fn read(scope: Read) -> Scopes {
        Scopes {
            scopes: vec![Scope::Read(Some(scope))],
        }
    }

    pub fn read_all() -> Scopes {
        Scopes {
            scopes: vec![Scope::Read(None)],
        }
    }

    pub fn write(scope: Write) -> Scopes {
        Scopes {
            scopes: vec![Scope::Write(Some(scope))],
        }
    }

    pub fn write_all() -> Scopes {
        Scopes {
            scopes: vec![Scope::Write(None)],
        }
    }

    pub fn follow() -> Scopes {
        Scopes {
            scopes: vec![Scope::Follow],
        }
    }

    pub fn push() -> Scopes {
        Scopes {
            scopes: vec![Scope::Push],
        }
    }

    pub fn admin_read(scope: AdminRead) -> Scopes {
        Scopes {
            scopes: vec![Scope::AdminRead(Some(scope))],
        }
    }

    pub fn admin_read_all() -> Scopes {
        Scopes {
            scopes: vec![Scope::AdminRead(None)],
        }
    }

    pub fn admin_write(scope: AdminWrite) -> Scopes {
        Scopes {
            scopes: vec![Scope::AdminWrite(Some(scope))],
        }
    }

    pub fn admin_write_all() -> Scopes {
        Scopes {
            scopes: vec![Scope::AdminWrite(None)],
        }
    }

    pub fn with(self, other: Scopes) -> Scopes {
        let mut new_scopes: Vec<Scope> = vec![];

        for scope in self.scopes {
            new_scopes.push(scope);
        }

        for scope in other.scopes {
            if !new_scopes.contains(&scope) {
                new_scopes.push(scope);
            }
        }

        Scopes { scopes: new_scopes }
    }
}

impl Default for Scopes {
    fn default() -> Scopes {
        Scopes::read_all()
    }
}

impl BitOr for Scopes {
    type Output = Scopes;

    fn bitor(self, other: Scopes) -> Self::Output {
        self.with(other)
    }
}

impl PartialEq for Scopes {
    fn eq(&self, other: &Scopes) -> bool {
        self.scopes == other.scopes
    }
}

impl fmt::Display for Scopes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut start = true;
        let scopes = {
            let mut scopes = self.scopes.iter().collect::<Vec<_>>();
            scopes.sort();
            scopes
        };

        for scope in &scopes {
            if !start {
                write!(f, " ")?;
            } else {
                start = false;
            }
            write!(f, "{}", &scope)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_scope_displaying() {
        let tests = [
            (Scope::Read(None), "read"),
            (Scope::Read(Some(Read::Accounts)), "read:accounts"),
            (Scope::Read(Some(Read::Blocks)), "read:blocks"),
            (Scope::Read(Some(Read::Favourites)), "read:favourites"),
            (Scope::Read(Some(Read::Filters)), "read:filters"),
            (Scope::Read(Some(Read::Follows)), "read:follows"),
            (Scope::Read(Some(Read::Lists)), "read:lists"),
            (Scope::Read(Some(Read::Mutes)), "read:mutes"),
            (Scope::Read(Some(Read::Notifications)), "read:notifications"),
            (Scope::Read(Some(Read::Bookmarks)), "read:bookmarks"),
            (Scope::Read(Some(Read::Search)), "read:search"),
            (Scope::Read(Some(Read::Statuses)), "read:statuses"),
            (Scope::Write(None), "write"),
            (Scope::Write(Some(Write::Accounts)), "write:accounts"),
            (Scope::Write(Some(Write::Blocks)), "write:blocks"),
            (Scope::Write(Some(Write::Favourites)), "write:favourites"),
            (Scope::Write(Some(Write::Filters)), "write:filters"),
            (Scope::Write(Some(Write::Follows)), "write:follows"),
            (Scope::Write(Some(Write::Lists)), "write:lists"),
            (Scope::Write(Some(Write::Media)), "write:media"),
            (Scope::Write(Some(Write::Mutes)), "write:mutes"),
            (Scope::Write(Some(Write::Notifications)), "write:notifications"),
            (Scope::Write(Some(Write::Reports)), "write:reports"),
            (Scope::Write(Some(Write::Bookmarks)), "write:bookmarks"),
            (Scope::Write(Some(Write::Statuses)), "write:statuses"),
            (Scope::Follow, "follow"),
            (Scope::Push, "push"),
            (Scope::AdminRead(None), "admin:read"),
            (Scope::AdminRead(Some(AdminRead::Accounts)), "admin:read:accounts"),
            (Scope::AdminRead(Some(AdminRead::Reports)), "admin:read:reports"),
            (Scope::AdminWrite(None), "admin:write"),
            (Scope::AdminWrite(Some(AdminWrite::Accounts)), "admin:write:accounts"),
            (Scope::AdminWrite(Some(AdminWrite::Reports)), "admin:write:reports"),
        ];

        for (value, expected) in tests {
            let result = value.to_string();
            assert_eq!(&result, expected);
        }
    }

    #[test]
    fn test_scopes_default() {
        let default: Scope = Default::default();
        assert_eq!(default, Scope::Read(None));
    }

    #[test]
    fn test_scopes_display() {
        let tests = [
            (Scopes::read(Read::Accounts) | Scopes::follow(), "read:accounts follow"),
            (
                Scopes::read(Read::Follows) | Scopes::read(Read::Accounts) | Scopes::write_all(),
                "read:accounts read:follows write",
            ),
        ];

        for (a, b) in &tests {
            assert_eq!(&format!("{}", a), b);
        }
    }

    #[test]
    fn test_scopes_bitor() {
        let tests = [
            (Scopes::read(Read::Accounts) | Scopes::follow(), "read:accounts follow"),
            (
                Scopes::read(Read::Follows) | Scopes::read(Read::Accounts) | Scopes::write_all(),
                "read:accounts read:follows write",
            ),
        ];

        for (a, b) in &tests {
            assert_eq!(&format!("{}", a), b);
        }
    }

    #[test]
    fn test_scopes_serialize_deserialize() {
        let tests = [
            (
                Scopes::read_all() | Scopes::write(Write::Notifications) | Scopes::follow(),
                "read write:notifications follow",
            ),
            (Scopes::follow() | Scopes::push(), "follow push"),
        ];

        for (a, b) in &tests {
            let ser = serde_json::to_string(&a).expect("Couldn't serialize Scopes");
            let expected = format!("\"{}\"", b);
            assert_eq!(&ser, &expected);

            let des: Scopes = serde_json::from_str(&ser).expect("Couldn't deserialize Scopes");
            assert_eq!(&des, a);
        }
    }

    #[test]
    fn test_scope_from_str() {
        let tests = [
            ("read", Scope::Read(None)),
            ("read:accounts", Scope::Read(Some(Read::Accounts))),
            ("read:blocks", Scope::Read(Some(Read::Blocks))),
            ("read:favourites", Scope::Read(Some(Read::Favourites))),
            ("read:filters", Scope::Read(Some(Read::Filters))),
            ("read:follows", Scope::Read(Some(Read::Follows))),
            ("read:lists", Scope::Read(Some(Read::Lists))),
            ("read:mutes", Scope::Read(Some(Read::Mutes))),
            ("read:notifications", Scope::Read(Some(Read::Notifications))),
            ("read:bookmarks", Scope::Read(Some(Read::Bookmarks))),
            ("read:search", Scope::Read(Some(Read::Search))),
            ("read:statuses", Scope::Read(Some(Read::Statuses))),
            ("write", Scope::Write(None)),
            ("write:accounts", Scope::Write(Some(Write::Accounts))),
            ("write:blocks", Scope::Write(Some(Write::Blocks))),
            ("write:favourites", Scope::Write(Some(Write::Favourites))),
            ("write:filters", Scope::Write(Some(Write::Filters))),
            ("write:follows", Scope::Write(Some(Write::Follows))),
            ("write:lists", Scope::Write(Some(Write::Lists))),
            ("write:media", Scope::Write(Some(Write::Media))),
            ("write:mutes", Scope::Write(Some(Write::Mutes))),
            ("write:notifications", Scope::Write(Some(Write::Notifications))),
            ("write:reports", Scope::Write(Some(Write::Reports))),
            ("write:statuses", Scope::Write(Some(Write::Statuses))),
            ("follow", Scope::Follow),
            ("push", Scope::Push),
            ("admin:read", Scope::AdminRead(None)),
            ("admin:read:accounts", Scope::AdminRead(Some(AdminRead::Accounts))),
            ("admin:read:reports", Scope::AdminRead(Some(AdminRead::Reports))),
            ("admin:write", Scope::AdminWrite(None)),
            ("admin:write:accounts", Scope::AdminWrite(Some(AdminWrite::Accounts))),
            ("admin:write:reports", Scope::AdminWrite(Some(AdminWrite::Reports))),
        ];
        for (source, expected) in &tests {
            let result = Scope::from_str(source).unwrap_or_else(|_| panic!("Couldn't parse '{}'", &source));
            assert_eq!(result, *expected);
        }
    }

    #[test]
    fn test_scopes_str_round_trip() {
        let original = "read write follow push";
        let scopes = Scopes::from_str(original).expect("Couldn't convert to Scopes");
        let result = format!("{}", scopes);
        assert_eq!(original, result);
    }
}
