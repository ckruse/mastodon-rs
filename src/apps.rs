use anyhow::Result;
use serde::Serialize;

use crate::{entities::Application, errors, Client, Scopes};

#[derive(Clone, Debug, Default, Serialize, PartialEq)]
pub struct AppRegistration {
    client_name: String,
    redirect_uris: String,
    scopes: Scopes,
    #[serde(skip_serializing_if = "Option::is_none")]
    website: Option<String>,
}

impl AppRegistration {
    pub fn builder() -> AppRegistrationBuilder {
        AppRegistrationBuilder::new()
    }

    pub fn scopes(&self) -> &Scopes {
        &self.scopes
    }

    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

#[derive(Clone, Debug, Default, Serialize, PartialEq)]
pub struct AppRegistrationBuilder {
    client_name: Option<String>,
    redirect_uris: Option<String>,
    scopes: Option<Scopes>,
    website: Option<String>,
}

const DEFAULT_REDIRECT_URI: &str = "urn:ietf:wg:oauth:2.0:oob";

impl AppRegistrationBuilder {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn client_name<I: Into<String>>(&mut self, name: I) -> &mut Self {
        self.client_name = Some(name.into());
        self
    }

    pub fn redirect_uris<I: Into<String>>(&mut self, uris: I) -> &mut Self {
        self.redirect_uris = Some(uris.into());
        self
    }

    pub fn scopes(&mut self, scopes: Scopes) -> &mut Self {
        self.scopes = Some(scopes);
        self
    }

    pub fn website<I: Into<String>>(&mut self, website: I) -> &mut Self {
        self.website = Some(website.into());
        self
    }

    pub fn build(self) -> errors::Result<AppRegistration> {
        Ok(AppRegistration {
            client_name: self
                .client_name
                .ok_or(errors::Error::ParameterMissing("client_name"))?
                .into(),
            redirect_uris: self.redirect_uris.unwrap_or_else(|| DEFAULT_REDIRECT_URI.into()).into(),
            scopes: self.scopes.unwrap_or_else(Scopes::read_all),
            website: self.website.map(|s| s.into()),
        })
    }
}

impl<'a> TryInto<AppRegistration> for AppRegistrationBuilder {
    type Error = errors::Error;

    fn try_into(self) -> errors::Result<AppRegistration> {
        Ok(self.build()?)
    }
}

impl Client {
    pub async fn create_app(&self, app_registration: &AppRegistration) -> Result<Application> {
        let url = url::Url::parse(&format!("{}/api/v1/apps", self.data.base))?;
        debug!("url: {:?}\n", url);

        let data = app_registration.to_querystring()?;
        let app = self.post_formdata(url.as_str(), data).await?;

        Ok(app)
    }

    pub async fn verify_app(&self) -> Result<Application> {
        let url = url::Url::parse(&format!("{}/api/v1/apps/verify_credentials", self.data.base))?;
        debug!("url: {:?}\n", url);

        self.ensure_token()?;
        let app = self.get(url.as_str()).await?;

        Ok(app)
    }
}

#[cfg(test)]
mod tests {
    use dotenv::dotenv;
    use std::env;

    use super::*;
    use crate::*;

    fn init() {
        dotenv().ok();
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn init_client() -> Result<Client> {
        let mut client_builder = ClientBuilder::new();
        client_builder.base(&env::var("MASTODON_RS_TEST_SERVER")?);
        client_builder.token(&env::var("BEARER_TOKEN")?);
        client_builder.build()
    }

    #[test]
    fn test_builder() {
        let builder = AppRegistration::builder();
        assert_eq!(builder, AppRegistrationBuilder::new());
    }

    #[test]
    fn test_scopes() {
        let mut builder = AppRegistration::builder();
        builder.client_name("Example").scopes(Scopes::all());

        let app = builder.build().expect("Couldn't build app registration");

        assert_eq!(app.scopes(), &Scopes::all());
    }

    #[test]
    fn test_set_all_parameters() {
        let mut builder = AppRegistrationBuilder::new();
        builder.client_name("foo-test");
        builder.redirect_uris("http://example.com");
        builder.scopes(Scopes::read_all() | Scopes::write_all());
        builder.website("https://example.com");

        let app = builder.build().expect("Couldn't build App");

        assert_eq!(
            app,
            AppRegistration {
                client_name: "foo-test".to_string(),
                redirect_uris: "http://example.com".to_string(),
                scopes: Scopes::read_all() | Scopes::write_all(),
                website: Some("https://example.com".to_string()),
            }
        );
    }

    #[test]
    #[should_panic]
    fn test_build_fails_if_no_client_name() {
        AppRegistration::builder().build().expect("no client-name");
    }

    #[test]
    #[should_panic]
    fn test_build_fails_if_no_client_name_but_other_params() {
        let mut builder = AppRegistration::builder();
        builder
            .website("https://example.com")
            .redirect_uris("https://example.com")
            .scopes(Scopes::all());
        builder.build().expect("no client-name");
    }

    #[test]
    fn test_try_into() {
        let app = AppRegistration {
            client_name: "foo-test".to_string(),
            redirect_uris: "http://example.com".to_string(),
            scopes: Scopes::all(),
            website: None,
        };

        let expected = app.clone();
        let result = app.try_into().expect("Couldn't convert into AppRegistration");

        assert_eq!(expected, result);
    }

    #[test]
    fn test_app_builder_try_into_app() {
        let mut builder = AppRegistration::builder();

        builder
            .client_name("foo-test")
            .redirect_uris("http://example.com")
            .scopes(Scopes::all());

        let expected = AppRegistration {
            client_name: "foo-test".to_string(),
            redirect_uris: "http://example.com".to_string(),
            scopes: Scopes::all(),
            website: None,
        };

        let result = builder.try_into().expect("couldn't convert into AppRegistration");

        assert_eq!(expected, result);
    }

    #[tokio::test]
    async fn creates_an_app() -> Result<()> {
        init();
        let client = init_client()?;
        let mut builder = AppRegistration::builder();
        builder.client_name("Mastodon-rs Test");
        builder.website("https://wwwtech.de/");
        builder.scopes(Scopes::all());

        let app_registration = builder.build()?;

        let app = client.create_app(&app_registration).await?;

        assert_eq!(app.name, "Mastodon-rs Test");
        assert_eq!(app.website, Some("https://wwwtech.de/".to_owned()));

        Ok(())
    }

    #[tokio::test]
    async fn verifies_an_app() -> Result<()> {
        init();
        let client = init_client()?;
        let mut builder = AppRegistration::builder();
        builder.client_name("Mastodon-rs Test");
        builder.scopes(Scopes::all());

        let _app = client.verify_app().await?;

        Ok(())
    }
}
