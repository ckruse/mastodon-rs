use anyhow::Result;

use crate::entities::{Account, Card, Context, Status};
use crate::Client;

impl Client {
    pub async fn status(&self, id: &str) -> Result<Status> {
        let url = url::Url::parse(&self.data.base)?.join("/api/v1/statuses/")?.join(id)?;

        debug!("url: {:?}\n", url);

        let status = self.get::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn status_context(&self, id: &str) -> Result<Context> {
        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/context", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let context = self.get::<Context>(url.as_str()).await?;
        Ok(context)
    }

    pub async fn reblogged_by(&self, id: &str) -> Result<Vec<Account>> {
        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/reblogged_by", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let accounts = self.get::<Vec<Account>>(url.as_str()).await?;
        Ok(accounts)
    }

    pub async fn favourited_by(&self, id: &str) -> Result<Vec<Account>> {
        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/favourited_by", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let accounts = self.get::<Vec<Account>>(url.as_str()).await?;
        Ok(accounts)
    }

    pub async fn favourite(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/favourite", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn unfavourite(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/unfavourite", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn reblog(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/reblog", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn unreblog(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/unreblog", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn bookmark(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/bookmark", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn unbookmark(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/unbookmark", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn mute(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/mute", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn unmute(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/unmute", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn pin(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/pin", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    pub async fn unpin(&self, id: &str) -> Result<Status> {
        self.ensure_token()?;

        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/unpin", self.data.base, id))?;
        debug!("url: {:?}\n", url);

        let status = self.post::<Status>(url.as_str()).await?;
        Ok(status)
    }

    #[deprecated]
    pub async fn card(&self, id: &str) -> Result<Card> {
        let url = url::Url::parse(&format!("{}/api/v1/statuses/{}/card", self.data.base, id))?;
        let card = self.get::<Card>(url.as_str()).await?;
        Ok(card)
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    use dotenv::dotenv;
    use std::env;

    fn init() {
        dotenv().ok();
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn init_client() -> Result<Client> {
        let mut client_builder = ClientBuilder::new();
        client_builder.base(&env::var("MASTODON_RS_TEST_SERVER")?);
        client_builder.build()
    }

    #[tokio::test]
    async fn queries_a_status() -> Result<()> {
        init();
        let client = init_client()?;

        let _status = client.status(&env::var("MASTODON_RS_TEST_STATUS_ID")?).await?;

        Ok(())
    }

    #[tokio::test]
    async fn queries_a_status_context() -> Result<()> {
        init();
        let client = init_client()?;
        let _status = client.status_context(&env::var("MASTODON_RS_TEST_STATUS_ID")?).await?;

        Ok(())
    }

    #[tokio::test]
    async fn get_a_list_of_rebloggers() -> Result<()> {
        init();
        let client = init_client()?;
        let _status = client.reblogged_by(&env::var("MASTODON_RS_TEST_STATUS_ID")?).await?;

        Ok(())
    }

    #[tokio::test]
    async fn get_a_list_of_favouriters() -> Result<()> {
        init();
        let client = init_client()?;
        let _status = client.favourited_by(&env::var("MASTODON_RS_TEST_STATUS_ID")?).await?;

        Ok(())
    }
}
