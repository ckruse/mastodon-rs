use anyhow::Result;

use crate::entities::Conversation;
use crate::{BasicTimelineParams, Client, Empty};

impl Client {
    pub async fn conversations(&self, params: Option<&BasicTimelineParams>) -> Result<Vec<Conversation>> {
        let url_str = format!("{}/api/v1/conversations", self.data.base);
        let mut url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        let conversations = self.get::<Vec<Conversation>>(url.as_str()).await?;
        Ok(conversations)
    }

    pub async fn delete_conversation(&self, id: &str) -> Result<()> {
        let url_str = format!("{}/api/v1/conversations/{}", self.data.base, id);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        self.delete::<Empty>(url.as_str()).await?;
        Ok(())
    }

    pub async fn mark_conversation_read(&self, id: &str) -> Result<Conversation> {
        let url_str = format!("{}/api/v1/conversations/{}/read", self.data.base, id);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        let conversation = self.post::<Conversation>(url.as_str()).await?;
        Ok(conversation)
    }
}
