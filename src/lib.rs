#[macro_use]
extern crate log;

use anyhow::{anyhow, Result};
use reqwest::Client as HttpClient;
use serde::{Deserialize, Serialize};

pub mod apps;
pub mod conversations;
pub mod entities;
pub mod errors;
pub mod lists;
pub mod scopes;
pub mod statuses;
pub mod timelines;

mod serialization;

pub use conversations::*;
pub use errors::*;
pub use lists::*;
pub use scopes::*;
pub use statuses::*;
pub use timelines::*;

use crate::serialization::bool_serialize;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Data {
    pub base: String,
    pub client_id: Option<String>,
    pub client_secret: Option<String>,
    pub redirect: Option<String>,
    pub token: Option<String>,
}

pub struct Client {
    client: HttpClient,
    pub data: Data,
}

#[derive(Default, Debug, Clone, Serialize)]
pub struct BasicTimelineParams {
    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub local: bool,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub since_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub min_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
}

impl BasicTimelineParams {
    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub(crate) struct Empty {}

impl Client {
    async fn get<T>(&self, url: &str) -> Result<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        let mut rq = self
            .client
            .get(url)
            .header("user-agent", format!("mastodon-rs/{}", env!("CARGO_PKG_VERSION")));

        if let Some(token) = &self.data.token {
            rq = rq.bearer_auth(token);
        }

        let rslt = rq.send().await?.json::<T>().await?;
        Ok(rslt)
    }

    async fn post<T>(&self, url: &str) -> Result<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        let mut rq = self
            .client
            .post(url)
            .header("user-agent", format!("mastodon-rs/{}", env!("CARGO_PKG_VERSION")));

        if let Some(token) = &self.data.token {
            rq = rq.bearer_auth(token);
        }

        let rslt = rq.send().await?.json::<T>().await?;

        Ok(rslt)
    }

    async fn post_formdata<T>(&self, url: &str, data: String) -> Result<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        let mut rq = self
            .client
            .post(url)
            .header("user-agent", format!("mastodon-rs/{}", env!("CARGO_PKG_VERSION")))
            .header("content-type", "application/x-www-form-urlencoded")
            .body(data);

        if let Some(token) = &self.data.token {
            rq = rq.bearer_auth(token);
        }

        let rslt = rq.send().await?.json::<T>().await?;

        Ok(rslt)
    }

    async fn put_formdata<T>(&self, url: &str, data: String) -> Result<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        let mut rq = self
            .client
            .put(url)
            .header("user-agent", format!("mastodon-rs/{}", env!("CARGO_PKG_VERSION")))
            .header("content-type", "application/x-www-form-urlencoded")
            .body(data);

        if let Some(token) = &self.data.token {
            rq = rq.bearer_auth(token);
        }

        let rslt = rq.send().await?.json::<T>().await?;

        Ok(rslt)
    }

    async fn delete<T>(&self, url: &str) -> Result<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        let mut rq = self
            .client
            .delete(url)
            .header("user-agent", format!("mastodon-rs/{}", env!("CARGO_PKG_VERSION")));

        if let Some(token) = &self.data.token {
            rq = rq.bearer_auth(token);
        }

        let rslt = rq.send().await?.json::<T>().await?;

        Ok(rslt)
    }

    fn ensure_token(&self) -> Result<(), errors::Error> {
        if self.data.token.is_none() {
            return Err(errors::Error::AuthRequired);
        }

        Ok(())
    }
}

impl From<Data> for Client {
    fn from(data: Data) -> Client {
        Client {
            client: HttpClient::new(),
            data,
        }
    }
}

pub struct ClientBuilder {
    client: Option<HttpClient>,
    base: Option<String>,
    client_id: Option<String>,
    client_secret: Option<String>,
    redirect: Option<String>,
    token: Option<String>,
}

impl ClientBuilder {
    pub fn new() -> ClientBuilder {
        ClientBuilder {
            client: None,
            base: None,
            client_id: None,
            client_secret: None,
            redirect: None,
            token: None,
        }
    }

    pub fn client(&mut self, client: HttpClient) -> &mut Self {
        self.client = Some(client);
        self
    }

    pub fn base(&mut self, base: &str) -> &mut Self {
        self.base = Some(base.to_owned());
        self
    }

    pub fn client_id(&mut self, client_id: &str) -> &mut Self {
        self.client_id = Some(client_id.to_owned());
        self
    }

    pub fn client_secret(&mut self, client_secret: &str) -> &mut Self {
        self.client_secret = Some(client_secret.to_owned());
        self
    }

    pub fn redirect(&mut self, redirect: &str) -> &mut Self {
        self.redirect = Some(redirect.to_owned());
        self
    }

    pub fn token(&mut self, token: &str) -> &mut Self {
        self.token = Some(token.to_owned());
        self
    }

    pub fn build(self) -> Result<Client> {
        if let Some(base) = self.base {
            Ok(Client {
                client: self.client.unwrap_or_else(HttpClient::new),
                data: Data {
                    base,
                    client_id: self.client_id,
                    client_secret: self.client_secret,
                    redirect: self.redirect,
                    token: self.token,
                },
            })
        } else {
            Err(anyhow!("Missing base URL"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_builds_from_data() {
        let data = Data {
            base: "foo".to_owned(),
            client_id: Some("bar".to_owned()),
            client_secret: Some("baz".to_owned()),
            redirect: None,
            token: Some("bar".to_owned()),
        };

        let client = Client::from(data);
        assert_eq!(client.data.base, "foo");
        assert_eq!(client.data.client_id, Some("bar".to_owned()));
        assert_eq!(client.data.client_secret, Some("baz".to_owned()));
        assert_eq!(client.data.redirect, None);
        assert_eq!(client.data.token, Some("bar".to_owned()));
    }

    #[test]
    fn it_builds_with_builder() -> Result<()> {
        let mut builder = ClientBuilder::new();
        builder.base("foo").client_id("bar").client_secret("baz").token("bar");

        let client = builder.build()?;

        assert_eq!(client.data.base, "foo");
        assert_eq!(client.data.client_id, Some("bar".to_owned()));
        assert_eq!(client.data.client_secret, Some("baz".to_owned()));
        assert_eq!(client.data.redirect, None);
        assert_eq!(client.data.token, Some("bar".to_owned()));

        return Ok(());
    }
}
