use anyhow::Result;
use serde::Serialize;

use crate::entities::{List, RepliesPolicy};
use crate::{Client, Empty};

#[derive(Debug, Clone, Serialize)]
pub struct NewList {
    pub title: String,
    pub replies_policy: RepliesPolicy,
}

impl NewList {
    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct UpdateList {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replies_policy: Option<RepliesPolicy>,
}

impl UpdateList {
    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

impl Client {
    pub async fn lists(&self) -> Result<Vec<List>> {
        let url_str = format!("{}/api/v1/lists", self.data.base);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        let lists = self.get::<Vec<List>>(url.as_str()).await?;
        Ok(lists)
    }

    pub async fn list(&self, id: &str) -> Result<List> {
        let url_str = format!("{}/api/v1/lists/{}", self.data.base, id);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        let list = self.get::<List>(url.as_str()).await?;
        Ok(list)
    }

    pub async fn create_list(&self, new_list: NewList) -> Result<List> {
        let url_str = format!("{}/api/v1/lists", self.data.base);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        let data = new_list.to_querystring()?;
        let list = self.post_formdata::<List>(url.as_str(), data).await?;
        Ok(list)
    }

    pub async fn update_list(&self, list: &List, update_list: UpdateList) -> Result<List> {
        let url_str = format!("{}/api/v1/lists/{}", self.data.base, list.id);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        let data = update_list.to_querystring()?;
        let list = self.put_formdata::<List>(url.as_str(), data).await?;
        Ok(list)
    }

    pub async fn delete_list(&self, list: &List) -> Result<()> {
        let url_str = format!("{}/api/v1/lists/{}", self.data.base, list.id);
        let url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        self.delete::<Empty>(url.as_str()).await?;
        Ok(())
    }
}
