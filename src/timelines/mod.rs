use anyhow::Result;
use serde::Serialize;

use crate::entities::Status;
use crate::serialization::bool_serialize;
use crate::{BasicTimelineParams, Client};

#[derive(Default, Debug, Clone, Serialize)]
pub struct PublicTimelineParams {
    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub local: bool,

    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub remote: bool,

    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub only_media: bool,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub since_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub min_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
}

impl PublicTimelineParams {
    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

#[derive(Default, Debug, Clone, Serialize)]
pub struct HashtagTimelineParams {
    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub local: bool,

    #[serde(skip_serializing_if = "bool_serialize::is_false")]
    #[serde(serialize_with = "bool_serialize::serialize")]
    pub only_media: bool,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub since_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub min_id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
}

impl HashtagTimelineParams {
    pub fn to_querystring(&self) -> Result<String> {
        Ok(serde_qs::to_string(&self)?)
    }
}

impl Client {
    pub async fn public_timeline(&self, params: Option<&PublicTimelineParams>) -> Result<Vec<Status>> {
        let mut url = url::Url::parse(&self.data.base)?.join("/api/v1/timelines/public")?;

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        debug!("url: {:?}\n", url);

        let statuses = self.get::<Vec<Status>>(url.as_str()).await?;
        Ok(statuses)
    }

    pub async fn hashtag_timeline(&self, hashtag: &str, params: Option<&PublicTimelineParams>) -> Result<Vec<Status>> {
        let url_str = format!("{}/api/v1/timelines/tag/{}", self.data.base, hashtag);
        let mut url = url::Url::parse(&url_str)?;

        debug!("url: {:?}\n", url);

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        let statuses = self.get::<Vec<Status>>(url.as_str()).await?;
        Ok(statuses)
    }

    pub async fn home_timeline(&self, params: Option<&BasicTimelineParams>) -> Result<Vec<Status>> {
        let url_str = format!("{}/api/v1/timelines/home", self.data.base);
        let mut url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        let statuses = self.get::<Vec<Status>>(url.as_str()).await?;
        Ok(statuses)
    }

    pub async fn list_timeline(&self, list_id: &str, params: Option<&BasicTimelineParams>) -> Result<Vec<Status>> {
        let url_str = format!("{}/api/v1/timelines/list/{}", self.data.base, list_id);
        let mut url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        let statuses = self.get::<Vec<Status>>(url.as_str()).await?;
        Ok(statuses)
    }

    #[deprecated]
    pub async fn direct_timeline(&self, params: Option<&BasicTimelineParams>) -> Result<Vec<Status>> {
        let url_str = format!("{}/api/v1/timelines/home", self.data.base);
        let mut url = url::Url::parse(&url_str)?;

        self.ensure_token()?;

        debug!("url: {:?}\n", url);

        if let Some(opts) = params {
            let qs = opts.to_querystring()?;
            url.set_query(Some(&qs));
        }

        let statuses = self.get::<Vec<Status>>(url.as_str()).await?;
        Ok(statuses)
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    use std::env;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn init_client() -> Result<Client> {
        let mut client_builder = ClientBuilder::new();
        client_builder.base(&env::var("MASTODON_RS_TEST_SERVER")?);
        client_builder.build()
    }

    #[tokio::test]
    async fn queries_federated_timeline() -> Result<()> {
        init();
        let client = init_client()?;

        let options = PublicTimelineParams {
            remote: true,
            ..Default::default()
        };
        let _statuses = client.public_timeline(Some(&options)).await?;

        Ok(())
    }

    #[tokio::test]
    async fn queries_wo_opts() -> Result<()> {
        init();
        let client = init_client()?;
        let _statuses = client.public_timeline(None).await?;

        Ok(())
    }

    #[tokio::test]
    async fn queries_local_timeline() -> Result<()> {
        init();
        let client = init_client()?;

        let options = PublicTimelineParams {
            local: true,
            ..Default::default()
        };
        let _statuses = client.public_timeline(Some(&options)).await?;

        Ok(())
    }

    #[tokio::test]
    async fn queries_hashtag_timeline() -> Result<()> {
        init();
        let client = init_client()?;
        let _statuses = client.hashtag_timeline("rustlang", None).await?;

        Ok(())
    }
}
