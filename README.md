# mastodon-rs

A library for the [Mastodon](https://joinmastodon.org/) [client API](https://docs.joinmastodon.org/client/intro/).

Be aware: this library is in early alpha stage, a lot of things are not yet implemented.

## Contributing

Just create a feature branch and send a PR. :-)
